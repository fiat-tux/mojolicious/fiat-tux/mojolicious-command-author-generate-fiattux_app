# NAME

Mojolicious::Command::Author::generate::fiattux\_app - Fiat Tux app generator command

# SYNOPSIS

    Usage: APPLICATION generate fiattux-app [OPTIONS] [NAME]
      mojo generate fiattux-app
      mojo generate fiattux-app TestApp
    Options:
      -h, --help   Show this summary of available options

# DESCRIPTION

[Mojolicious::Command::Author::generate::fiattux\_app](https://metacpan.org/pod/Mojolicious::Command::Author::generate::fiattux_app) generates application directory structures for fully functional
[Mojolicious](https://metacpan.org/pod/Mojolicious) applications, with Fiat Tux flavor.

Forked from [Mojolicious::Command::Author::generate::app](https://metacpan.org/pod/Mojolicious::Command::Author::generate::app)
See ["COMMANDS" in Mojolicious::Commands](https://metacpan.org/pod/Mojolicious::Commands#COMMANDS) for a list of commands that are available by default.

# ATTRIBUTES

[Mojolicious::Command::Author::generate::fiattux\_app](https://metacpan.org/pod/Mojolicious::Command::Author::generate::fiattux_app) inherits all attributes from [Mojolicious::Command](https://metacpan.org/pod/Mojolicious::Command) and implements the
following new ones.

## description

    my $description = $app->description;
    $app            = $app->description('Foo');

Short description of this command, used for the command list.

## usage

    my $usage = $app->usage;
    $app      = $app->usage('Foo');

Usage information for this command, used for the help screen.

# METHODS

[Mojolicious::Command::Author::generate::fiattux\_app](https://metacpan.org/pod/Mojolicious::Command::Author::generate::fiattux_app) inherits all methods from [Mojolicious::Command](https://metacpan.org/pod/Mojolicious::Command) and implements the
following new ones.

## run

    $app->run(@ARGV);

Run this command.

# SEE ALSO

[Mojolicious](https://metacpan.org/pod/Mojolicious), [Mojolicious::Guides](https://metacpan.org/pod/Mojolicious::Guides), [https://mojolicious.org](https://mojolicious.org).

# AUTHOR

Luc Didry <luc@didry.org>

# LICENSE

Copyright (C) Luc Didry.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.
