# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package Mojolicious::Command::Author::generate::fiattux_app;
use Mojo::Base 'Mojolicious::Command', -signatures;
use Mojo::Util qw(class_to_file class_to_path decamelize);

has description => 'Generate Mojolicious Fiat Tux application directory structure';
has usage       => sub { shift->extract_usage };

our $VERSION = "0.14";

sub run ($self, $class = 'MyApp') {

    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    $year += 1900;

    # Script
    my $name = class_to_file $class;
    $self->render_to_rel_file('mojo',    "$name/script/$name",   { class => $class });
    $self->render_to_rel_file('mounter', "$name/script/mounter", { class => $class });
    $self->chmod_rel_file("$name/script/$name",   0744);
    $self->chmod_rel_file("$name/script/mounter", 0744);

    # Application class
    my $app = class_to_path $class;
    $self->render_to_rel_file('appclass', "$name/lib/$app", { class => $class, name => $name });

    # mounter class
    $self->render_to_rel_file('mounterclass', "$name/lib/Mounter.pm", { class => $class, name => $name });

    # Config file (using the default moniker)
    $self->render_to_rel_file('config', "$name/$name.yml.template", { class => $class, name => $name });

    # Default config
    my $default_config = "${class}::DefaultConfig";
    my $path           = class_to_path $default_config;
    $self->render_to_rel_file('default_config', "$name/lib/$path", { class => $class, name => $name });

    # Controller
    my $controller = "${class}::Controller::Misc";
    $path          = class_to_path $controller;
    $self->render_to_rel_file('controller', "$name/lib/$path", { class => $controller });

    # Model
    my $model = "${class}::Db";
    $path     = class_to_path $model;
    $self->render_to_rel_file('model', "$name/lib/$path", { class => $model });
    Mojo::File->new("$name/lib/$class/Db")->make_path;

    # Helpers
    my $helpers = "${class}::Plugin::Helpers";
    $path       = class_to_path $helpers;
    $self->render_to_rel_file('helpers', "$name/lib/$path", { class => $helpers });

    # Command
    my $command = "${class}::Command::theme";
    $path       = class_to_path $command;
    $self->render_to_rel_file('theme', "$name/lib/$path", { class => $command, app => $class, name => $name, year => $year });

    # Test
    $self->render_to_rel_file('test', "$name/t/basic.t", { class => $class });

    # Static files
    Mojo::File->new("$name/themes/default/public/js/")->make_path;
    Mojo::File->new("$name/themes/default/public/css/")->make_path;
    Mojo::File->new("$name/themes/default/public/img/")->make_path;
    Mojo::File->new("$name/themes/default/public/js/@{[decamelize $class]}.js")->spurt('// vim:set sw=4 ts=4 sts=4 ft=javascript expandtab:');
    Mojo::File->new("$name/themes/default/public/css/@{[decamelize $class]}.css")->spurt('/* vim:set sw=4 ts=4 sts=4 ft=css expandtab: */');

    # I18N file
    $self->render_to_rel_file('I18N', "$name/themes/default/lib/$class/I18N.pm", { class => $class });
    Mojo::File->new("$name/themes/default/lib/$class/I18N")->make_path;

    # Templates
    $self->render_to_rel_file('layout', "$name/themes/default/templates/layouts/default.html.ep", { class => $class, name => $name });
    $self->render_to_rel_file('index',  "$name/themes/default/templates/misc/index.html.ep", { class => $class, decam_class => @{[decamelize $class]} });

    # cpanfile
    $self->render_to_rel_file('cpanfile', "$name/cpanfile");

    # Makefile
    $self->render_to_rel_file('makefile', "$name/Makefile", { class => $class, name => $name });

    # Nginx
    $self->render_to_rel_file('nginx', "$name/utilities/$name.nginx.conf", { name => $name });

    # Services
    $self->render_to_rel_file('service', "$name/utilities/$name.service",         { class => $class, name => $name });
    $self->render_to_rel_file('minion',  "$name/utilities/$name-minion@.service", { class => $class, name => $name });

    # Migrations
    Mojo::File->new("$name/utilities/migrations.sql")->spurt("-- 1 up\n-- 1 down");

    # .gitignore
    $self->render_to_rel_file('gitignore', "$name/.gitignore");

    # Readme
    $self->render_to_rel_file('readme', "$name/README.md", { class => $class, name => $name });

    # License
    $self->render_to_rel_file('license', "$name/LICENSE", { class => $class, year => $year });
}

1;

=pod

=encoding utf-8

=head1 NAME

Mojolicious::Command::Author::generate::fiattux_app - Fiat Tux app generator command

=head1 SYNOPSIS

  Usage: APPLICATION generate fiattux-app [OPTIONS] [NAME]
    mojo generate fiattux-app
    mojo generate fiattux-app TestApp
  Options:
    -h, --help   Show this summary of available options

=head1 DESCRIPTION

L<Mojolicious::Command::Author::generate::fiattux_app> generates application directory structures for fully functional
L<Mojolicious> applications, with Fiat Tux flavor.

Forked from L<Mojolicious::Command::Author::generate::app>
See L<Mojolicious::Commands/"COMMANDS"> for a list of commands that are available by default.

=head1 ATTRIBUTES

L<Mojolicious::Command::Author::generate::fiattux_app> inherits all attributes from L<Mojolicious::Command> and implements the
following new ones.

=head2 description

  my $description = $app->description;
  $app            = $app->description('Foo');

Short description of this command, used for the command list.

=head2 usage

  my $usage = $app->usage;
  $app      = $app->usage('Foo');

Usage information for this command, used for the help screen.

=head1 METHODS

L<Mojolicious::Command::Author::generate::fiattux_app> inherits all methods from L<Mojolicious::Command> and implements the
following new ones.

=head2 run

  $app->run(@ARGV);

Run this command.

=head1 SEE ALSO

L<Mojolicious>, L<Mojolicious::Guides>, L<https://mojolicious.org>.

=head1 AUTHOR

Luc Didry E<lt>luc@didry.orgE<gt>

=head1 LICENSE

Copyright (C) Luc Didry.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__DATA__

@@ mojo
#!/usr/bin/env perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:

use strict;
use warnings;

use Mojo::File qw(curfile);
use lib curfile->dirname->sibling('lib')->to_string;
use Mojolicious::Commands;

# Start command line interface for application
Mojolicious::Commands->start_app('<%= $class %>');

@@ mounter
#!/usr/bin/env perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:

use strict;
use warnings;

use Mojo::File qw(curfile);
use lib curfile->dirname->sibling('lib')->to_string;
use Mojolicious::Commands;

# Start command line interface for application
Mojolicious::Commands->start_app('Mounter');

@@ appclass
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package <%= $class %>;
use Mojo::Base 'Mojolicious', -signatures;
use Mojo::File qw(curfile);

use <%= $class %>::DefaultConfig qw($default_config);

$ENV{MOJO_CONFIG} //= '<%= $name %>.yml';

sub startup ($self) {

    # Load configuration from config file
    my $config = $self->plugin('NotYAMLConfig' => { default => $default_config });

    die 'You need to change the secrets settings in <%= $name %>.yml!' unless ($config->{secrets} && $config->{secrets}->[0] ne 'foobarbaz');

    $self->config('prefix', $config->{prefix}.'/') unless substr($config->{prefix}, -1) eq '/';

    # Configure the application
    $self->secrets($config->{secrets});

    # Commands
    push @{$self->commands->namespaces}, '<%= $class %>::Command';

    # Internationalization
    my $lib = curfile->sibling('..', 'themes', $config->{theme}, 'lib');
    eval qq(use lib "$lib");
    $self->plugin('I18N');

    # Themes handling
    $self->plugin('FiatTux::Themes');

    # Debug
    $self->plugin('DebugDumperHelper');

    # Compress static assets
    $self->plugin('GzipStatic');

    # Assets Cache headers
    $self->plugin('StaticCache');

    # CSP Headers
    $self->plugin('CSPHeader', csp => $config->{csp}) if $config->{csp};

    # Helpers
    $self->plugin('<%= $class %>::Plugin::Helpers');
    $self->plugin('FiatTux::Helpers');

    # Configure sessions
    my $sessions = Mojolicious::Sessions->new;
    $sessions->cookie_name('<%= $name %>');
    $sessions->cookie_path($self->config('prefix'));
    $self->sessions($sessions);

    # Default layout
    $self->defaults(layout => 'default');

    # Router
    my $r = $self->routes;

    # Normal route to controller
    $r->get('/')
      ->to('Misc#index')
      ->name('index');

    $r->get('/lang/:l')
      ->to('Misc#change_lang')
      ->name('lang');
}

1;

@@ mounterclass
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package Mounter;
use Mojo::Base 'Mojolicious', -signatures;
use Mojo::File qw(curfile);

use <%= $class %>::DefaultConfig qw($default_config);

# This method will run once at server start
sub startup ($self) {
    # Load configuration from config file
    my $cfile = curfile->sibling('..' , '<%= $name %>.yml');
    if (defined $ENV{MOJO_CONFIG}) {
        $cfile = Mojo::File->new($ENV{MOJO_CONFIG});
        unless (-e $cfile->to_abs) {
            $cfile = curfile->sibling('..', $ENV{MOJO_CONFIG});
        }
    }
    my $config = $self->plugin('NotYAMLConfig' =>
        {
            file    => $cfile,
            default => $default_config
        }
    );

    # Themes handling
    $self->plugin('FiatTux::Themes');

    # Compress static assets
    $self->plugin('GzipStatic');

    # Assets Cache headers
    $app->plugin('StaticCache');

    # CSP Headers
    $self->plugin('CSPHeader', csp => $config->{csp}) if $config->{csp};

    # Helpers
    $self->plugin('<%= $class %>::Plugin::Helpers');
    $self->plugin('FiatTux::Helpers');

    $self->plugin('Mount' => {$config->{prefix} => curfile->sibling('..', 'script', '<%= $name %>')});
}

1;

@@ config
---
# ###################
# Hypnotoad settings
# ###################
# see http://mojolicio.us/perldoc/Mojo/Server/Hypnotoad for a full list of settings
hypnotoad:
  # Array of IP addresses and ports you want to listen to
  # You can specify a unix socket too, like 'http+unix://%2Ftmp%2F<%= $name %>.sock'
  listen:
    - http://127.0.0.1:8081
  # If you use <%= $class %> behind a reverse proxy like Nginx, you want to let proxy to true
  # If you use <%= $class %> directly, set it to false
  proxy: true

# ######################
# <%= $class %> settings
# ######################
# Name of the instance, displayed next to the logo
# optional, default is <%= $class %>
# instance_name: <%= $class %>

# URL sub-directory in which you want <%= $class %> to be accessible
# example: you want to have <%= $class %> under https://example.org/<%= $name %>/
# => set prefix to '/<%= $name %>' or to '/<%= $name %>/', it doesn't matter
# optional, defaut is /
# prefix: /

# Array of random strings used to encrypt cookies
# MANDATORY: change it!
secrets:
  - foobarbaz

# Choose a theme. See the available themes in `themes` directory
# Optional, default is 'default'
# theme: default

# Broadcast_message which will displayed on the index page
# optional, no default
# broadcast_message: Maintenance

###############
# Mail settings
###############

# Mail configuration
# See https://metacpan.org/pod/Mojolicious::Plugin::EmailMailer#CONFIGURATION
# optional, default to sendmail method with no arguments
# mail:
#   how: smtp
#   howargs:
#     hosts:
#       - smtp.example.org

# Email sender address
# optional, default to no-reply@<%= $name %>.example.org
# mail_sender: no-reply@<%= $name %>.example.org

#############
# DB settings
#############
# These are the credentials to access the PostgreSQL database
# MANDATORY
db:
  database: <%= $name %>
  host: localhost
  # optional, default is 5432
  # port: 5432
  user: DBUSER
  pwd: DBPASSWORD
  # https://mojolicious.org/perldoc/Mojo/Pg#max_connections
  # optional, default is 1
  # max_connections: 1

#######################
# HTTP Headers settings
#######################

# Content-Security-Policy header that will be sent by <%= $class %>
# Set to '' to disable CSP header
# https://content-security-policy.com/ provides a good documentation about CSP.
# https://report-uri.com/home/generate provides a tool to generate a CSP header.
# optional, default is an empty string
# csp: ''

# X-Frame-Options header that will be sent by <%= $class %>
# Valid values are: 'DENY', 'SAMEORIGIN', 'ALLOW-FROM https://example.com/'
# Set to '' to disable X-Frame-Options header
# See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
# Please note that this will add a "frame-ancestors" directive to the CSP header (see above) accordingly
# to the chosen setting (See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/frame-ancestors)
# optional, default is 'DENY'
# x_frame_options: DENY

# X-Content-Type-Options that will be sent by <%= $class %>
# See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options
# Set to '' to disable X-Content-Type-Options header
# optional, default is 'nosniff'
# x_content_type_options: nosniff

# X-XSS-Protection that will be sent by <%= $class %>
# See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection
# Set to '' to disable X-XSS-Protection header
# optional, default is '1; mode=block'
# x_xss_protection: 1; mode=block

@@ default_config
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package <%= $class %>::DefaultConfig;
require Exporter;
@ISA = qw(Exporter);
@EXPORT_OK = qw($default_config);
our $default_config = {
    instance_name => <%= $class %>,
    prefix        => '/',
    secrets       => [ 'foobarbaz' ],
    theme         => 'default',
    mail          => {
        how     => 'smtp',
        howargs => [ 'smtp.example.org']
    },
    mail_sender            => 'no-reply@<%= $name %>.example.org',
    x_frame_options        => 'DENY',
    x_content_type_options => 'nosniff',
    x_xss_protection       => '1; mode=block',
};

1;

@@ controller
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package <%= $class %>;
use Mojo::Base 'Mojolicious::Controller', -signatures;

sub index ($c) {
    $c->render(
        template => 'misc/index',
        msg      => $c->l('Welcome to the Mojolicious real-time web framework!')
    );
}

sub change_lang ($c, $l) {
    if ($c->iso639_native_name($l)) {
        $c->cookie($c->app->moniker.'_lang' => $l, { path => $c->config('prefix') });
    }

    if ($c->req->headers->referrer
        && Mojo::URL->new($c->req->headers->referrer)->host eq $c->req->host) {
        return $c->redirect_to($c->req->headers->referrer);
    } else {
        return $c->redirect_to('/');
    }
}

1;

@@ model
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package <%= $class %>;
use Mojo::Base -base, -signatures;
use Mojo::Collection;
use Data::Structure::Util qw(unbless);

has 'table';
has 'app';
has 'id';

sub new ($c, @params) {
    my $c = shift;
    $c = $c->SUPER::new(@params);

    if (defined $c->id) {
        $c = $c->find_by_('id', $c->id);
    }

    return $c;
}

sub create ($c, $h) {
    my ($fields, $prepare, $values) = $c->map_fields_for_insert($h);

    my $r = $c->app->pg->db->query('INSERT INTO '.$c->table.' ('.$fields.') VALUES ('.$prepare.') RETURNING *', @{$values});

    if ($r->rows == 1) {
        $c->map_fields_to_attr($r->hash);
    }
    return $c;
}

sub update ($c, $h) {
    my ($fields, $params) = $c->map_fields_for_update($h);
    push @{$params}, $c->id;

    my $q = join(', ', @{$fields});

    my $r = $c->app->pg->db->query('UPDATE '.$c->table.' SET '.$q.' WHERE id = ? RETURNING *;', @{$params});

    if ($r->rows == 1) {
        $c->map_fields_to_attr($r->hash);
    }
    return $c;
}

sub delete ($c) {
    # The CASCADE deletion is handled by the db schema where needed
    return $c->app->pg->db->query('DELETE FROM '.$c->table.' WHERE id = ?', $c->id);
}

sub as_struct ($c) {
    delete $c->{app};
    delete $c->{table};

    return unbless($c);
}

# Find a record and map fields as attributes if found
sub find_by_ ($c, $i, $j) {
    my $r = $c->app->pg->db->query('SELECT * FROM '.$c->table.' WHERE '.$i.' = ?;', $j);

    $c->map_fields_to_attr($r->hash) if $r->rows == 1;

    return $c;
}

# Find a record by multiple fields and map fields as attributes if found
sub find_by_fields_ ($c, $h) {
    my ($fields, $values) = $c->map_attr_for_select($h);
    my $r = $c->app->pg->db->query('SELECT * FROM '.$c->table.' WHERE '.$fields, @{$values});

    $c->map_fields_to_attr($r->hash) if $r->rows == 1;

    return $c;
}

# Map fields as attributes
sub map_fields_to_attr ($c, $h) {
    while (my ($k, $v) = each %{$h}) {
        # Add each field as object's attribute
        $c->{$k} = $v;
    }

    return $c;
}

# Transform hash reference as two array refs:
# ['field1 = ?, 'field2' = ?'] and [value1, value2]
sub map_fields_for_update ($c, $f) {
    my @fields = ();
    for my $key (keys %{$f}) {
        push @fields, $key.' = ?';
    }
    my @params = values %{$f};

    return (\@fields, \@params);
}

# Transform hash reference as two strings and an array ref:
# 'field1, field2', '?, ?', [value1, value2]
sub map_fields_for_insert ($c, $f) {
    my @i = ();
    my @fields = keys %{$f};
    my @params = values %{$f};
    for (1..scalar(@fields)) {
        push @i, '?';
    }

    return(join(', ', @fields), join(', ', @i), \@params);
}

# Transform array reference as a string and an array ref providing
# corresponding object attribute value;
# 'field1 = ? AND field2 = ?' and [attribute_field1, attribute_field2]
sub map_attr_for_select ($c, $h) {
    my @fields = ();
    my @values = ();
    for my $key (keys %{$h}) {
        push @fields, $key.' = ?';
        push @values, $h->{$key};
    }

    return (join(' AND ', @fields), \@values);
}

1;

@@ helpers
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package <%= $class %>;
use Mojo::Base 'Mojolicious::Plugin', -signatures;
use Mojo::Pg;

sub register ($self, $app, @args) {
    $app->plugin('PgURLHelper');

    $app->helper(dbi => \&_pg);

    # Database migrations
    my $migrations = Mojo::Pg::Migrations->new(pg => $app->dbi);
    $migrations->from_file('utilities/migrations.sql')->migrate($migrations->latest);
}

sub _pg ($c) {
    my $pgdb = $c->config('db');
    my $port = (defined $pgdb->{port}) ? $pgdb->{port}: 5432;
    my $addr = $c->pg_url({
        host     => $pgdb->{host},
        port     => $port,
        database => $pgdb->{database},
        user     => $pgdb->{user},
        pwd      => $pgdb->{pwd}
    });
    state $pg = Mojo::Pg->new($addr);
    $pg->max_connections($pgdb->{max_connections}) if defined $pgdb->{max_connections};
    return $pg;
}

1;

@@ theme
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package <%= $class %>;
use Mojo::Base 'Mojolicious::Commands', -signatures;
use Mojo::File qw(curfile);
use FindBin qw($Bin);
use File::Spec qw(catfile cat dir);

has description => 'Create new theme skeleton.';
has usage       => sub { shift->extract_usage };
has message     => sub { shift->extract_usage . "\nCreate new theme skeleton:\n" };
has namespaces  => sub { ['<%= $class %>'] };

sub run ($c, $name) {
    my $home = curfile->sibling('..', 'themes', $name);

    unless (-d $home) {
        # Create skeleton
        $home->sibling('public')->make_path;
        $home->sibling('templates', 'layouts')->make_path;
        $home->sibling('lib', '<%= $app %>', 'I18N')->make_path;

        my $i18n = <<EOF;
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package <%= $app %>::I18N;

use base 'Locale::Maketext';
use Mojo::File qw(curfile);
use Locale::Maketext::Lexicon {
    _auto => 1,
    _decode => 1,
    _style  => 'gettext',
    '*' => [
        Gettext => curfile->sibling('I18N', '*.po')->to_string,
        Gettext => Mojo::File->new(\$app_dir, 'themes', 'default', lib', '<%= $app %>', 'I18N', '*.po')->to_string
    ]
};

use vars qw(\$app_dir);
BEGIN {
    use Cwd;
    my \$app_dir = getcwd;
}

1;
EOF

        open my $f, '>', $home->sibling('lib', '<%= $app %>', 'I18N.pm') or die "Unable to open $home/lib/<%= $app %>/I18N.pm: $!";
        print $f $i18n;
        close $f;

        my $makefile = <<EOF;
POT=lib/<%= $app %>/I18N/$home.pot
EN=lib/<%= $app %>/I18N/en.po
SEDOPTS=-e "s\@SOME DESCRIPTIVE TITLE\@<%= $app %> language file\@" \\
                -e "s\@YEAR THE PACKAGE'S COPYRIGHT HOLDER\@<%= $year %>\@" \\
                -e "s\@CHARSET\@utf8\@" \\
                -e "s\@the PACKAGE package\@the <%= $app %> package\@" \\
                -e '/^\\#\\. (/{N;/\\n\\#\\. (/{N;/\\n.*\\.\\.\\/default\\//{s/\\#\\..*\\n.*\\#\\./\\#. (/g}}}' \\
                -e '/^\\#\\. (/{N;/\\n.*\\.\\.\\/default\\//{s/\\n/ /}}'
SEDOPTS2=-e '/^\\#.*\\.\\.\\/default\\//,+3d'
XGETTEXT=carton exec ../../local/bin/xgettext.pl
CARTON=carton exec

locales:
                \$(XGETTEXT) -D templates -D ../default/templates -o \$(POT) 2>/dev/null
                \$(XGETTEXT) -D templates -D ../default/templates -o \$(EN) 2>/dev/null
                sed \$(SEDOPTS) -i \$(POT)
                sed \$(SEDOPTS2) -i \$(POT)
                sed \$(SEDOPTS) -i \$(EN)
                sed \$(SEDOPTS2) -i \$(EN)
EOF

        open $f, '>', File::Spec->catfile($home, 'Makefile') or die "Unable to open $home/Makefile: $!";
        print $f $makefile;
        close $f;
    } else {
        say "$name theme already exists. Aborting.";
        exit 1;
    }
}

%= "=encoding utf8\n\n=head1 NAME\n\nTest::Command::theme - Create new theme skeleton.\n\n=head1 SYNOPSIS\n\n  Usage: script/test theme THEME_NAME\n\n  Your new theme will be available in the themes directory.\n=cut"

1;

@@ test
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

my $t = Test::Mojo->new('<%= $class %>');

$t->get_ok('/')
  ->status_is(200)
  ->content_like(qr/Mojolicious/i);

done_testing();

@@ I18N
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package <%= $class %>::I18N;

use base 'Locale::Maketext';
use Mojo::File qw(curfile);
use Locale::Maketext::Lexicon {
    _auto   => 1,
    _decode => 1,
    _style  => 'gettext',
    '*'     => [ Gettext  => curfile->sibling('I18N', '*.po')->to_string ]
};

1;

@@ layout
%% my $lang = $self->languages;
%%    $lang =~ s/-(.*)/_\U$1/;
<!DOCTYPE html>
<html>
    <head>
        %%= tag 'title', $title // '<%= $class %>'
        %%= tag 'link', rel => 'icon', type => 'image/png', href => url_for('/img/favicon.png')

        %%= tag 'meta', content => $title // '<%= $class %>', name => 'description'

        %%= tag 'meta' , content => $title // '<%= $class %>'           , property => 'og:title'
        %%= tag 'meta' , content => $lang                               , property => 'og:locale'
        %%= tag 'meta' , content => 'website'                           , property => 'og:type'
        %%= tag 'meta' , content => url_with(current_route)->to_abs     , property => 'og:url'
        %%= tag 'meta' , content => url_for('/img/favicon.png')->to_abs , property => 'og:image'
        %%= tag 'meta' , content => $title // '<%= $class %>'           , property => 'og:description'

        %%= tag 'meta' , content => 'width=device-width, initial-scale=1', name => 'viewport'

        %%= stylesheet 'css/<%= $name %>.min.css'
        %%= javascript 'js/<%= $name %>.min.js', defer => undef
    </head>
    <body>
        <header>
            %% if ($self->config('broadcast')) {
                %%= tag div => ( class => 'broadcast' ) => begin
                    %%= $self->config('broadcast')
                %% end
            %% }
        </header>
        <main>
            <%%= content %>
        </main>
    </body>
</html>

@@ index
%% title l('Welcome');
<h1>
    <%%= $msg %>
</h1>
<p>
    %%== l('This page was generated from the template %1 and the layout %2.', '<code>themes/default/templates/misc/index.html.ep</code>', '<code>themes/default/templates/layouts/default.html.ep</code>')
</p>

@@ cpanfile
requires 'Mojolicious';
requires 'Mojolicious::Plugin::CSPHeader';
requires 'Mojolicious::Plugin::DebugDumperHelper';
requires 'Mojolicious::Plugin::EmailMailer';
requires 'Mojolicious::Plugin::GzipStatic';
requires 'Mojolicious::Plugin::I18N';
requires 'Mojolicious::Plugin::PgURLHelper';
requires 'Mojolicious::Plugin::StaticCache';
requires 'Minion';
requires 'Mojo::Pg';
requires 'Locale::Maketext';
requires 'Locale::Maketext::Extract';
requires 'DateTime';
requires 'DateTime::Format::Pg';

requires 'Mojolicious::Plugin::FiatTux::Helpers', '== 0.12', url => 'https://framagit.org/fiat-tux/mojolicious/fiat-tux/mojolicious-plugin-fiattux-helpers/-/archive/0.12/mojolicious-plugin-fiattux-helpers-0.12.tar.gz';
requires 'Mojolicious::Plugin::FiatTux::Themes',  '== 0.02', url => 'https://framagit.org/fiat-tux/mojolicious/fiat-tux/mojolicious-plugin-fiattux-themes/-/archive/0.02/mojolicious-plugin-fiattux-themes-0.02.tar.gz';

# Mojolicious optional deps
feature 'optional_deps' => sub {
    requires 'Cpanel::JSON::XS';
    requires 'EV';
    requires 'IO::Socket::Socks';
    requires 'Net::DNS::Native';
    requires 'Role::Tiny';
};

feature 'test' => sub {
    requires 'Devel::Cover';
    requires 'B::Debug';
};

@@ makefile
EXTRACTDIR=-D lib -D themes/default/templates
POT=themes/default/lib/<%= $class %>/I18N/<%= $name %>.pot
ENPO=themes/default/lib/<%= $class %>/I18N/en.po
XGETTEXT=carton exec local/bin/xgettext.pl -u

.PHONY: locales dev minion ldap check-syntax just-test test

locales:
	$(XGETTEXT) $(EXTRACTDIR) -o $(POT) 2>/dev/null
	$(XGETTEXT) $(EXTRACTDIR) -o $(ENPO) 2>/dev/null

dev:
	carton exec morbo script/<%= $name %> --listen http://0.0.0.0:3000 --watch themes/ --watch lib/ --watch script/ --watch <%= $name %>.yml

minion:
	carton exec script/<%= $name %> minion worker -- -m development

check-syntax:
	find lib/ themes/ -name \*.pm -exec carton exec perl -Ilib -c {} \;
	find t/ -name \*.t -exec carton exec perl -Ilib -c {} \;

ldap:
	sudo docker run --privileged -d -p 389:389 rroemhild/test-openldap; exit 0

just-test:
	@PERL5OPT='-Ilib/' HARNESS_PERL_SWITCHES='-MDevel::Cover=+ignore,local' MOJO_CONFIG=t/<%= $name %>.yml carton exec script/<%= $name %> test

cover:
	PERL5OPT='-Ilib/' carton exec cover --ignore_re '^local'

test: check-syntax just-test

@@ nginx
server {
    listen 80;
    listen [::]:80;

    server_name <%= $name %>.exemple.org;

    access_log  /var/log/nginx/<%= $name %>.exemple.access.log;
    error_log   /var/log/nginx/<%= $name %>.exemple.error.log;

    location / {
        include proxy_params;
        proxy_http_version 1.1;
        proxy_pass http://127.0.0.1:8081;
        # We expect the downsteam server to redirect to the right hostname, so don't do any rewrites here.
        proxy_redirect off;
    }
}

@@ service
[Unit]
Description=<%= $class %>
Documentation=https://framagit.org/fiat-tux/hat-softwares/<%= $name %>/
Requires=network.target postgresql.service
After=network.target postgresql.service

[Service]
Type=forking
User=www-data
RemainAfterExit=yes
WorkingDirectory=/var/www/<%= $name %>
PIDFile=/var/www/<%= $name %>/script/hypnotoad.pid
ExecStart=/usr/local/bin/carton exec hypnotoad script/mounter
ExecStop=/usr/local/bin/carton exec hypnotoad -s script/mounter
ExecReload=/usr/local/bin/carton exec hypnotoad script/mounter
SyslogIdentifier=<%= $name %>

[Install]
WantedBy=multi-user.target

@@ minion
[Unit]
Description=<%= $class %> job queue
Documentation=https://framagit.org/fiat-tux/hat-softwares/<%= $name %>/
After=<%= $name %>.service

[Service]
Type=simple
User=www-data
WorkingDirectory=/var/www/<%= $name %>/
ExecStart=/usr/local/bin/carton exec script/<%= $name %> minion worker -m production -j %i
SyslogIdentifier=<%= $name %>-minion

[Install]
WantedBy=multi-user.target

@@ gitignore
local/*
cover_db/*
*.yml
*.bak
*.swp
script/*.pid
themes/*
!themes/default
!themes/default/*

@@ readme
# <%= $class %>

<%= $class %> is...

# Install

Please go to the [wiki](https://framagit.org/fiat-tux/hat-softwares/<%= $name %>/wikis/installation)

## License

Licensed under the GNU AGPLv3, see the [LICENSE file](LICENSE).

## Authors

Please go to the [wiki](https://framagit.org/fiat-tux/hat-softwares/<%= $name %>/wikis/authors)

## Powered by

* Mojolicious
* Mojo::Pg
* Minion

@@ license
                    GNU AFFERO GENERAL PUBLIC LICENSE
                       Version 3, 19 November 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

                            Preamble

  The GNU Affero General Public License is a free, copyleft license for
software and other kinds of works, specifically designed to ensure
cooperation with the community in the case of network server software.

  The licenses for most software and other practical works are designed
to take away your freedom to share and change the works.  By contrast,
our General Public Licenses are intended to guarantee your freedom to
share and change all versions of a program--to make sure it remains free
software for all its users.

  When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
them if you wish), that you receive source code or can get it if you
want it, that you can change the software or use pieces of it in new
free programs, and that you know you can do these things.

  Developers that use our General Public Licenses protect your rights
with two steps: (1) assert copyright on the software, and (2) offer
you this License which gives you legal permission to copy, distribute
and/or modify the software.

  A secondary benefit of defending all users' freedom is that
improvements made in alternate versions of the program, if they
receive widespread use, become available for other developers to
incorporate.  Many developers of free software are heartened and
encouraged by the resulting cooperation.  However, in the case of
software used on network servers, this result may fail to come about.
The GNU General Public License permits making a modified version and
letting the public access it on a server without ever releasing its
source code to the public.

  The GNU Affero General Public License is designed specifically to
ensure that, in such cases, the modified source code becomes available
to the community.  It requires the operator of a network server to
provide the source code of the modified version running there to the
users of that server.  Therefore, public use of a modified version, on
a publicly accessible server, gives the public access to the source
code of the modified version.

  An older license, called the Affero General Public License and
published by Affero, was designed to accomplish similar goals.  This is
a different license, not a version of the Affero GPL, but Affero has
released a new version of the Affero GPL which permits relicensing under
this license.

  The precise terms and conditions for copying, distribution and
modification follow.

                       TERMS AND CONDITIONS

  0. Definitions.

  "This License" refers to version 3 of the GNU Affero General Public License.

  "Copyright" also means copyright-like laws that apply to other kinds of
works, such as semiconductor masks.

  "The Program" refers to any copyrightable work licensed under this
License.  Each licensee is addressed as "you".  "Licensees" and
"recipients" may be individuals or organizations.

  To "modify" a work means to copy from or adapt all or part of the work
in a fashion requiring copyright permission, other than the making of an
exact copy.  The resulting work is called a "modified version" of the
earlier work or a work "based on" the earlier work.

  A "covered work" means either the unmodified Program or a work based
on the Program.

  To "propagate" a work means to do anything with it that, without
permission, would make you directly or secondarily liable for
infringement under applicable copyright law, except executing it on a
computer or modifying a private copy.  Propagation includes copying,
distribution (with or without modification), making available to the
public, and in some countries other activities as well.

  To "convey" a work means any kind of propagation that enables other
parties to make or receive copies.  Mere interaction with a user through
a computer network, with no transfer of a copy, is not conveying.

  An interactive user interface displays "Appropriate Legal Notices"
to the extent that it includes a convenient and prominently visible
feature that (1) displays an appropriate copyright notice, and (2)
tells the user that there is no warranty for the work (except to the
extent that warranties are provided), that licensees may convey the
work under this License, and how to view a copy of this License.  If
the interface presents a list of user commands or options, such as a
menu, a prominent item in the list meets this criterion.

  1. Source Code.

  The "source code" for a work means the preferred form of the work
for making modifications to it.  "Object code" means any non-source
form of a work.

  A "Standard Interface" means an interface that either is an official
standard defined by a recognized standards body, or, in the case of
interfaces specified for a particular programming language, one that
is widely used among developers working in that language.

  The "System Libraries" of an executable work include anything, other
than the work as a whole, that (a) is included in the normal form of
packaging a Major Component, but which is not part of that Major
Component, and (b) serves only to enable use of the work with that
Major Component, or to implement a Standard Interface for which an
implementation is available to the public in source code form.  A
"Major Component", in this context, means a major essential component
(kernel, window system, and so on) of the specific operating system
(if any) on which the executable work runs, or a compiler used to
produce the work, or an object code interpreter used to run it.

  The "Corresponding Source" for a work in object code form means all
the source code needed to generate, install, and (for an executable
work) run the object code and to modify the work, including scripts to
control those activities.  However, it does not include the work's
System Libraries, or general-purpose tools or generally available free
programs which are used unmodified in performing those activities but
which are not part of the work.  For example, Corresponding Source
includes interface definition files associated with source files for
the work, and the source code for shared libraries and dynamically
linked subprograms that the work is specifically designed to require,
such as by intimate data communication or control flow between those
subprograms and other parts of the work.

  The Corresponding Source need not include anything that users
can regenerate automatically from other parts of the Corresponding
Source.

  The Corresponding Source for a work in source code form is that
same work.

  2. Basic Permissions.

  All rights granted under this License are granted for the term of
copyright on the Program, and are irrevocable provided the stated
conditions are met.  This License explicitly affirms your unlimited
permission to run the unmodified Program.  The output from running a
covered work is covered by this License only if the output, given its
content, constitutes a covered work.  This License acknowledges your
rights of fair use or other equivalent, as provided by copyright law.

  You may make, run and propagate covered works that you do not
convey, without conditions so long as your license otherwise remains
in force.  You may convey covered works to others for the sole purpose
of having them make modifications exclusively for you, or provide you
with facilities for running those works, provided that you comply with
the terms of this License in conveying all material for which you do
not control copyright.  Those thus making or running the covered works
for you must do so exclusively on your behalf, under your direction
and control, on terms that prohibit them from making any copies of
your copyrighted material outside their relationship with you.

  Conveying under any other circumstances is permitted solely under
the conditions stated below.  Sublicensing is not allowed; section 10
makes it unnecessary.

  3. Protecting Users' Legal Rights From Anti-Circumvention Law.

  No covered work shall be deemed part of an effective technological
measure under any applicable law fulfilling obligations under article
11 of the WIPO copyright treaty adopted on 20 December 1996, or
similar laws prohibiting or restricting circumvention of such
measures.

  When you convey a covered work, you waive any legal power to forbid
circumvention of technological measures to the extent such circumvention
is effected by exercising rights under this License with respect to
the covered work, and you disclaim any intention to limit operation or
modification of the work as a means of enforcing, against the work's
users, your or third parties' legal rights to forbid circumvention of
technological measures.

  4. Conveying Verbatim Copies.

  You may convey verbatim copies of the Program's source code as you
receive it, in any medium, provided that you conspicuously and
appropriately publish on each copy an appropriate copyright notice;
keep intact all notices stating that this License and any
non-permissive terms added in accord with section 7 apply to the code;
keep intact all notices of the absence of any warranty; and give all
recipients a copy of this License along with the Program.

  You may charge any price or no price for each copy that you convey,
and you may offer support or warranty protection for a fee.

  5. Conveying Modified Source Versions.

  You may convey a work based on the Program, or the modifications to
produce it from the Program, in the form of source code under the
terms of section 4, provided that you also meet all of these conditions:

    a) The work must carry prominent notices stating that you modified
    it, and giving a relevant date.

    b) The work must carry prominent notices stating that it is
    released under this License and any conditions added under section
    7.  This requirement modifies the requirement in section 4 to
    "keep intact all notices".

    c) You must license the entire work, as a whole, under this
    License to anyone who comes into possession of a copy.  This
    License will therefore apply, along with any applicable section 7
    additional terms, to the whole of the work, and all its parts,
    regardless of how they are packaged.  This License gives no
    permission to license the work in any other way, but it does not
    invalidate such permission if you have separately received it.

    d) If the work has interactive user interfaces, each must display
    Appropriate Legal Notices; however, if the Program has interactive
    interfaces that do not display Appropriate Legal Notices, your
    work need not make them do so.

  A compilation of a covered work with other separate and independent
works, which are not by their nature extensions of the covered work,
and which are not combined with it such as to form a larger program,
in or on a volume of a storage or distribution medium, is called an
"aggregate" if the compilation and its resulting copyright are not
used to limit the access or legal rights of the compilation's users
beyond what the individual works permit.  Inclusion of a covered work
in an aggregate does not cause this License to apply to the other
parts of the aggregate.

  6. Conveying Non-Source Forms.

  You may convey a covered work in object code form under the terms
of sections 4 and 5, provided that you also convey the
machine-readable Corresponding Source under the terms of this License,
in one of these ways:

    a) Convey the object code in, or embodied in, a physical product
    (including a physical distribution medium), accompanied by the
    Corresponding Source fixed on a durable physical medium
    customarily used for software interchange.

    b) Convey the object code in, or embodied in, a physical product
    (including a physical distribution medium), accompanied by a
    written offer, valid for at least three years and valid for as
    long as you offer spare parts or customer support for that product
    model, to give anyone who possesses the object code either (1) a
    copy of the Corresponding Source for all the software in the
    product that is covered by this License, on a durable physical
    medium customarily used for software interchange, for a price no
    more than your reasonable cost of physically performing this
    conveying of source, or (2) access to copy the
    Corresponding Source from a network server at no charge.

    c) Convey individual copies of the object code with a copy of the
    written offer to provide the Corresponding Source.  This
    alternative is allowed only occasionally and noncommercially, and
    only if you received the object code with such an offer, in accord
    with subsection 6b.

    d) Convey the object code by offering access from a designated
    place (gratis or for a charge), and offer equivalent access to the
    Corresponding Source in the same way through the same place at no
    further charge.  You need not require recipients to copy the
    Corresponding Source along with the object code.  If the place to
    copy the object code is a network server, the Corresponding Source
    may be on a different server (operated by you or a third party)
    that supports equivalent copying facilities, provided you maintain
    clear directions next to the object code saying where to find the
    Corresponding Source.  Regardless of what server hosts the
    Corresponding Source, you remain obligated to ensure that it is
    available for as long as needed to satisfy these requirements.

    e) Convey the object code using peer-to-peer transmission, provided
    you inform other peers where the object code and Corresponding
    Source of the work are being offered to the general public at no
    charge under subsection 6d.

  A separable portion of the object code, whose source code is excluded
from the Corresponding Source as a System Library, need not be
included in conveying the object code work.

  A "User Product" is either (1) a "consumer product", which means any
tangible personal property which is normally used for personal, family,
or household purposes, or (2) anything designed or sold for incorporation
into a dwelling.  In determining whether a product is a consumer product,
doubtful cases shall be resolved in favor of coverage.  For a particular
product received by a particular user, "normally used" refers to a
typical or common use of that class of product, regardless of the status
of the particular user or of the way in which the particular user
actually uses, or expects or is expected to use, the product.  A product
is a consumer product regardless of whether the product has substantial
commercial, industrial or non-consumer uses, unless such uses represent
the only significant mode of use of the product.

  "Installation Information" for a User Product means any methods,
procedures, authorization keys, or other information required to install
and execute modified versions of a covered work in that User Product from
a modified version of its Corresponding Source.  The information must
suffice to ensure that the continued functioning of the modified object
code is in no case prevented or interfered with solely because
modification has been made.

  If you convey an object code work under this section in, or with, or
specifically for use in, a User Product, and the conveying occurs as
part of a transaction in which the right of possession and use of the
User Product is transferred to the recipient in perpetuity or for a
fixed term (regardless of how the transaction is characterized), the
Corresponding Source conveyed under this section must be accompanied
by the Installation Information.  But this requirement does not apply
if neither you nor any third party retains the ability to install
modified object code on the User Product (for example, the work has
been installed in ROM).

  The requirement to provide Installation Information does not include a
requirement to continue to provide support service, warranty, or updates
for a work that has been modified or installed by the recipient, or for
the User Product in which it has been modified or installed.  Access to a
network may be denied when the modification itself materially and
adversely affects the operation of the network or violates the rules and
protocols for communication across the network.

  Corresponding Source conveyed, and Installation Information provided,
in accord with this section must be in a format that is publicly
documented (and with an implementation available to the public in
source code form), and must require no special password or key for
unpacking, reading or copying.

  7. Additional Terms.

  "Additional permissions" are terms that supplement the terms of this
License by making exceptions from one or more of its conditions.
Additional permissions that are applicable to the entire Program shall
be treated as though they were included in this License, to the extent
that they are valid under applicable law.  If additional permissions
apply only to part of the Program, that part may be used separately
under those permissions, but the entire Program remains governed by
this License without regard to the additional permissions.

  When you convey a copy of a covered work, you may at your option
remove any additional permissions from that copy, or from any part of
it.  (Additional permissions may be written to require their own
removal in certain cases when you modify the work.)  You may place
additional permissions on material, added by you to a covered work,
for which you have or can give appropriate copyright permission.

  Notwithstanding any other provision of this License, for material you
add to a covered work, you may (if authorized by the copyright holders of
that material) supplement the terms of this License with terms:

    a) Disclaiming warranty or limiting liability differently from the
    terms of sections 15 and 16 of this License; or

    b) Requiring preservation of specified reasonable legal notices or
    author attributions in that material or in the Appropriate Legal
    Notices displayed by works containing it; or

    c) Prohibiting misrepresentation of the origin of that material, or
    requiring that modified versions of such material be marked in
    reasonable ways as different from the original version; or

    d) Limiting the use for publicity purposes of names of licensors or
    authors of the material; or

    e) Declining to grant rights under trademark law for use of some
    trade names, trademarks, or service marks; or

    f) Requiring indemnification of licensors and authors of that
    material by anyone who conveys the material (or modified versions of
    it) with contractual assumptions of liability to the recipient, for
    any liability that these contractual assumptions directly impose on
    those licensors and authors.

  All other non-permissive additional terms are considered "further
restrictions" within the meaning of section 10.  If the Program as you
received it, or any part of it, contains a notice stating that it is
governed by this License along with a term that is a further
restriction, you may remove that term.  If a license document contains
a further restriction but permits relicensing or conveying under this
License, you may add to a covered work material governed by the terms
of that license document, provided that the further restriction does
not survive such relicensing or conveying.

  If you add terms to a covered work in accord with this section, you
must place, in the relevant source files, a statement of the
additional terms that apply to those files, or a notice indicating
where to find the applicable terms.

  Additional terms, permissive or non-permissive, may be stated in the
form of a separately written license, or stated as exceptions;
the above requirements apply either way.

  8. Termination.

  You may not propagate or modify a covered work except as expressly
provided under this License.  Any attempt otherwise to propagate or
modify it is void, and will automatically terminate your rights under
this License (including any patent licenses granted under the third
paragraph of section 11).

  However, if you cease all violation of this License, then your
license from a particular copyright holder is reinstated (a)
provisionally, unless and until the copyright holder explicitly and
finally terminates your license, and (b) permanently, if the copyright
holder fails to notify you of the violation by some reasonable means
prior to 60 days after the cessation.

  Moreover, your license from a particular copyright holder is
reinstated permanently if the copyright holder notifies you of the
violation by some reasonable means, this is the first time you have
received notice of violation of this License (for any work) from that
copyright holder, and you cure the violation prior to 30 days after
your receipt of the notice.

  Termination of your rights under this section does not terminate the
licenses of parties who have received copies or rights from you under
this License.  If your rights have been terminated and not permanently
reinstated, you do not qualify to receive new licenses for the same
material under section 10.

  9. Acceptance Not Required for Having Copies.

  You are not required to accept this License in order to receive or
run a copy of the Program.  Ancillary propagation of a covered work
occurring solely as a consequence of using peer-to-peer transmission
to receive a copy likewise does not require acceptance.  However,
nothing other than this License grants you permission to propagate or
modify any covered work.  These actions infringe copyright if you do
not accept this License.  Therefore, by modifying or propagating a
covered work, you indicate your acceptance of this License to do so.

  10. Automatic Licensing of Downstream Recipients.

  Each time you convey a covered work, the recipient automatically
receives a license from the original licensors, to run, modify and
propagate that work, subject to this License.  You are not responsible
for enforcing compliance by third parties with this License.

  An "entity transaction" is a transaction transferring control of an
organization, or substantially all assets of one, or subdividing an
organization, or merging organizations.  If propagation of a covered
work results from an entity transaction, each party to that
transaction who receives a copy of the work also receives whatever
licenses to the work the party's predecessor in interest had or could
give under the previous paragraph, plus a right to possession of the
Corresponding Source of the work from the predecessor in interest, if
the predecessor has it or can get it with reasonable efforts.

  You may not impose any further restrictions on the exercise of the
rights granted or affirmed under this License.  For example, you may
not impose a license fee, royalty, or other charge for exercise of
rights granted under this License, and you may not initiate litigation
(including a cross-claim or counterclaim in a lawsuit) alleging that
any patent claim is infringed by making, using, selling, offering for
sale, or importing the Program or any portion of it.

  11. Patents.

  A "contributor" is a copyright holder who authorizes use under this
License of the Program or a work on which the Program is based.  The
work thus licensed is called the contributor's "contributor version".

  A contributor's "essential patent claims" are all patent claims
owned or controlled by the contributor, whether already acquired or
hereafter acquired, that would be infringed by some manner, permitted
by this License, of making, using, or selling its contributor version,
but do not include claims that would be infringed only as a
consequence of further modification of the contributor version.  For
purposes of this definition, "control" includes the right to grant
patent sublicenses in a manner consistent with the requirements of
this License.

  Each contributor grants you a non-exclusive, worldwide, royalty-free
patent license under the contributor's essential patent claims, to
make, use, sell, offer for sale, import and otherwise run, modify and
propagate the contents of its contributor version.

  In the following three paragraphs, a "patent license" is any express
agreement or commitment, however denominated, not to enforce a patent
(such as an express permission to practice a patent or covenant not to
sue for patent infringement).  To "grant" such a patent license to a
party means to make such an agreement or commitment not to enforce a
patent against the party.

  If you convey a covered work, knowingly relying on a patent license,
and the Corresponding Source of the work is not available for anyone
to copy, free of charge and under the terms of this License, through a
publicly available network server or other readily accessible means,
then you must either (1) cause the Corresponding Source to be so
available, or (2) arrange to deprive yourself of the benefit of the
patent license for this particular work, or (3) arrange, in a manner
consistent with the requirements of this License, to extend the patent
license to downstream recipients.  "Knowingly relying" means you have
actual knowledge that, but for the patent license, your conveying the
covered work in a country, or your recipient's use of the covered work
in a country, would infringe one or more identifiable patents in that
country that you have reason to believe are valid.

  If, pursuant to or in connection with a single transaction or
arrangement, you convey, or propagate by procuring conveyance of, a
covered work, and grant a patent license to some of the parties
receiving the covered work authorizing them to use, propagate, modify
or convey a specific copy of the covered work, then the patent license
you grant is automatically extended to all recipients of the covered
work and works based on it.

  A patent license is "discriminatory" if it does not include within
the scope of its coverage, prohibits the exercise of, or is
conditioned on the non-exercise of one or more of the rights that are
specifically granted under this License.  You may not convey a covered
work if you are a party to an arrangement with a third party that is
in the business of distributing software, under which you make payment
to the third party based on the extent of your activity of conveying
the work, and under which the third party grants, to any of the
parties who would receive the covered work from you, a discriminatory
patent license (a) in connection with copies of the covered work
conveyed by you (or copies made from those copies), or (b) primarily
for and in connection with specific products or compilations that
contain the covered work, unless you entered into that arrangement,
or that patent license was granted, prior to 28 March 2007.

  Nothing in this License shall be construed as excluding or limiting
any implied license or other defenses to infringement that may
otherwise be available to you under applicable patent law.

  12. No Surrender of Others' Freedom.

  If conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot convey a
covered work so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you may
not convey it at all.  For example, if you agree to terms that obligate you
to collect a royalty for further conveying from those to whom you convey
the Program, the only way you could satisfy both those terms and this
License would be to refrain entirely from conveying the Program.

  13. Remote Network Interaction; Use with the GNU General Public License.

  Notwithstanding any other provision of this License, if you modify the
Program, your modified version must prominently offer all users
interacting with it remotely through a computer network (if your version
supports such interaction) an opportunity to receive the Corresponding
Source of your version by providing access to the Corresponding Source
from a network server at no charge, through some standard or customary
means of facilitating copying of software.  This Corresponding Source
shall include the Corresponding Source for any work covered by version 3
of the GNU General Public License that is incorporated pursuant to the
following paragraph.

  Notwithstanding any other provision of this License, you have
permission to link or combine any covered work with a work licensed
under version 3 of the GNU General Public License into a single
combined work, and to convey the resulting work.  The terms of this
License will continue to apply to the part which is the covered work,
but the work with which it is combined will remain governed by version
3 of the GNU General Public License.

  14. Revised Versions of this License.

  The Free Software Foundation may publish revised and/or new versions of
the GNU Affero General Public License from time to time.  Such new versions
will be similar in spirit to the present version, but may differ in detail to
address new problems or concerns.

  Each version is given a distinguishing version number.  If the
Program specifies that a certain numbered version of the GNU Affero General
Public License "or any later version" applies to it, you have the
option of following the terms and conditions either of that numbered
version or of any later version published by the Free Software
Foundation.  If the Program does not specify a version number of the
GNU Affero General Public License, you may choose any version ever published
by the Free Software Foundation.

  If the Program specifies that a proxy can decide which future
versions of the GNU Affero General Public License can be used, that proxy's
public statement of acceptance of a version permanently authorizes you
to choose that version for the Program.

  Later license versions may give you additional or different
permissions.  However, no additional obligations are imposed on any
author or copyright holder as a result of your choosing to follow a
later version.

  15. Disclaimer of Warranty.

  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY
OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM
IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

  16. Limitation of Liability.

  IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS
THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY
GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE
USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF
DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD
PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),
EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGES.

  17. Interpretation of Sections 15 and 16.

  If the disclaimer of warranty and limitation of liability provided
above cannot be given local legal effect according to their terms,
reviewing courts shall apply local law that most closely approximates
an absolute waiver of all civil liability in connection with the
Program, unless a warranty or assumption of liability accompanies a
copy of the Program in return for a fee.

                     END OF TERMS AND CONDITIONS

            How to Apply These Terms to Your New Programs

  If you develop a new program, and you want it to be of the greatest
possible use to the public, the best way to achieve this is to make it
free software which everyone can redistribute and change under these terms.

  To do so, attach the following notices to the program.  It is safest
to attach them to the start of each source file to most effectively
state the exclusion of warranty; and each file should have at least
the "copyright" line and a pointer to where the full notice is found.

    <%= $class %>
    Copyright (C) <%= $year %>  Luc Didry

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

  If your software can interact with users remotely through a computer
network, you should also make sure that it provides a way for users to
get its source.  For example, if your program is a web application, its
interface could display a "Source" link that leads users to an archive
of the code.  There are many ways you could offer source, and different
solutions will be better for different programs; see section 13 for the
specific requirements.

  You should also get your employer (if you work as a programmer) or school,
if any, to sign a "copyright disclaimer" for the program, if necessary.
For more information on this, and how to apply and follow the GNU AGPL, see
<http://www.gnu.org/licenses/>.
